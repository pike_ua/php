<?php
/*
* Гравець  кидає  4  шестигранні  кубики.  Успіхом  вважається  випадок,  коли  на  кубику 
* випало  5  або  6.  Вивести  які  шанси  гравця  отримати  лише  1  успіх,  лише  2  успіху,  лише 
* 3  успіху  та  лише  4  успіху. 
* Приклади:  на  кубиках  випало  3,  5,  2,  1  –  це  1  успіх.   На  кубиках  випало  2,  5,  6,  6  –  це  3 
* успіху. 
*/

header('Content-Type: text/html; charset=utf-8');
$allVariants = pow(6,4);
$goodVariants = 0;
$data = array(1,2,3,4,5,6);
$success1 = 0;
$success2 = 0;
$success3 = 0;
$success4 = 0;
	
for($first = 1; $first <= count($data); ++$first){
	for($second = 1; $second <= count($data); ++$second){
		for($third = 1; $third <= count($data); ++$third){
			for($fourth = 1; $fourth <= count($data); ++$fourth){
				$temp = $first . '-' . $second . '-' . $third . '-' . $fourth ;
				$matches = substr_count($temp, '5') + substr_count($temp, '6');
				if($matches == 1){
					++$success1;
				}elseif($matches == 2){
					++$success2;
				}elseif($matches == 3){
					++$success3;
				}elseif($matches == 4){
					++$success4;
				}
			}
		}
	}
}

$chance1 = number_format($success1 / $allVariants, 3);
$chance2 = number_format($success2 / $allVariants, 3);
$chance3 = number_format($success3 / $allVariants, 3);
$chance4 = number_format($success4 / $allVariants, 3);

echo 'Шанс на 1 успіх: ' . $chance1 . ' (' . $success1 . ' / ' . $allVariants . ')<br>';
echo 'Шанс на 2 успіха: ' . $chance2 . ' (' . $success2 . ' / ' . $allVariants . ')<br>';
echo 'Шанс на 3 успіха: ' . $chance3 . ' (' . $success3 . ' / ' . $allVariants . ')<br>';
echo 'Шанс на 4 успіха: ' . $chance4 . ' (' . $success4 . ' / ' . $allVariants . ')<br>';