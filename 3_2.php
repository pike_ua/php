<?php

/* Модифікувати  скрипт  з  частини  1  таким  чином,  щоб  коди  йшли  по  порядку.  Тобто 
*  перший  код  AA0000,  потім  AA0001,  …,  AA9999,  AB0000,  …,  AF9999,  BA0000,  … 
*/

$letters = array('A', 'B', 'C', 'D', 'E', 'F' );
	
if(!is_dir('codes')){
	mkdir('codes', 0777);
	$sys_file = fopen('./codes/sys.txt', 'w+');
}
	
if(file_exists('./codes/sys.txt')){
	$sys_file = fopen('./codes/sys.txt', 'r+');
}else{
	$sys_file = fopen('./codes/sys.txt', 'w'); 
}
	
$sys_content = fread($sys_file, filesize('./codes/sys.txt'));

if($sys_content == ''){
	$clientName = 'AA0001';
	fwrite($sys_file, $clientName);
	$customer_file = touch('./codes/' . $clientName . '.txt');
}else{
	preg_match_all('#([A-F]{2})([\d]{4})#', $sys_content, $matches);

	if($matches[2][0] == '9999'){
		$nextClientLetters = ++$matches[1][0];
		$nameLetters = $nextClientLetters;
		$nameDigits = '0001';
	}else{
		$nameLetters = $matches[1][0];
		$nextClientDigits = ++$matches[2][0];
		$nameDigits = str_pad($nextClientDigits, 4, '0', STR_PAD_LEFT);
	}
				
	$clientName = $nameLetters . $nameDigits;
	$customer_file = touch('./codes/' . $clientName . '.txt');
	unlink('./codes/sys.txt');
	$sys_file = fopen('./codes/sys.txt', 'w');
	$sys_data = fwrite($sys_file, $clientName);
	fclose($sys_file);
}